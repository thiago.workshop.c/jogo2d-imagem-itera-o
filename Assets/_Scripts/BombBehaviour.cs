﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//classe destinada ao comportamento da bomba
public class BombBehaviour : MonoBehaviour
{
    #region References
    AudioSource audioSourceRef;
    GameObject playerRef;
    #endregion

    #region Pitch
    float pitchModify;
    public bool debugPitch = false;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        audioSourceRef = GetComponent<AudioSource>();
        playerRef = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePitch();
    }
    //calculando update do pitch atravez da distancia entre o jogador e o listener 
    private void UpdatePitch()
    {
        float minDistance = audioSourceRef.minDistance;
        float maxDistance = audioSourceRef.maxDistance;

        //pegando  a distancia entre o objeto e o jogador
        float distanceFromListener = Vector2.Distance(this.gameObject.transform.position, playerRef.transform.position);
        //verificando se a distancia atual é maior que MaxDistance(3.5) 
        if (distanceFromListener >= maxDistance)
        {
            //pitch retorna ao ponto normal
            audioSourceRef.pitch = 1;
        }
        else
        {  //pitch modificado a cada iteração de aproximidade
            pitchModify = ((maxDistance - distanceFromListener) / 2.5f) + 1;
            audioSourceRef.pitch = pitchModify;
            if (debugPitch)
            {
                Debug.Log("pitchModify :" + pitchModify);
            }
        }
    }


}
