﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HelicopterBehavior : MonoBehaviour
{
    public GameObject videoPanel;
    public VideoPlayer video;

    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player") {
            videoPanel.SetActive(true);
            video.loopPointReached += EndReached;
        }
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        vp.playbackSpeed = vp.playbackSpeed / 10.0f;
        Debug.LogWarning("Video terminou");
        video.Stop();
        SceneManager.LoadScene(0);


    }
}
