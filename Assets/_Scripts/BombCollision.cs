﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCollision : MonoBehaviour
{
    public Animator animator;
    public AudioSource audioSrc;
    public AudioClip audioClip;
    private void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //verifica colisao com player
        if (collision.collider.name == "Player")
        {
            //adiciona o audioclip(som) ao audio src
            audioSrc.clip = audioClip;
            //toca apenas uma vez o audioclip carregado no audiosrc
            audioSrc.PlayOneShot(audioClip);

            Debug.Log("Cabum!!!");

            animator.SetBool("explode", true);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().health--;
            this.gameObject.GetComponent<EdgeCollider2D>().enabled = false;
            Invoke("DestroyBombObject", 1);
        }

    }

    void DestroyBombObject()
    {
        Destroy(this.gameObject);
    }

}
