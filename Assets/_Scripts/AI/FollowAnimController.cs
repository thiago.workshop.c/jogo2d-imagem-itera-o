﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowAnimController : MonoBehaviour
{
    Animator animator;
    Vector2 movement;

    PlayerMovement playerMovement;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        playerMovement = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerMovement.canBeFollow)
        {
            GetInputAxis();
            SetAnimParameters();
        }
        
    }

    private void SetAnimParameters()
    {
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
    }

    private void GetInputAxis()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
    }
}
