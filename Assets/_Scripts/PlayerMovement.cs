﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    #region Movements
    public float moveSpeed = 5f;
    Vector2 movement;
    public bool canBeFollow = true;
    #endregion

    #region Physics
    public Rigidbody2D rb;
    #endregion

    #region Animations
    public Animator animator;
    #endregion

    public bool playerIsDead = false;

    // Update is called once per frame
    void Update()
    {
        if (!playerIsDead)
        {
            GetInputAxis();
            SetAnimParameters();
        }
        else
        {
            canBeFollow = false;
        }    
    }

    void FixedUpdate()
    {
        if (!playerIsDead)
        {
            MovePlayer();
        }        
    }

    private void MovePlayer()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }

    private void SetAnimParameters()
    {
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
    }

    private void GetInputAxis()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
    }
}
