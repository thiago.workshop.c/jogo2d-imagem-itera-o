﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UIGameOverManager : MonoBehaviour
{
    public GameObject uiGameOver;

    public void ShowUI()
    {
        uiGameOver.SetActive(true);
    }

    public void HideUI()
    {
        uiGameOver.SetActive(false);
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
